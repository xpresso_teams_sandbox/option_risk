"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging


# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="option_risk",
                   level=logging.INFO)



import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from datetime import date, timedelta
from matplotlib import cm
import matplotlib
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import DatastreamDSWS as dsws
import app.QUANTFUNCTIONSAUG as qnt


class DSWSAdapter:

    def __init__(self, username, password):
        self.username = username
        self.password = password
        DSWSAdapter.ds = dsws.Datastream(username=self.username, password=self.password)
        # print(DSWSAdapter.ds.tokenResp)

    def GetPriceHistForTicker(self, tickerList, fieldList, startDate):
        df = DSWSAdapter.ds.get_data(tickers=tickerList, fields=fieldList, start=startDate, end='-5D')
        return pd.DataFrame(df)

    def GetAnalyticsForTicker(self, tickerList, fieldList):
        df = DSWSAdapter.ds.get_data(tickers=tickerList, fields=fieldList, kind=0)
        return pd.DataFrame(df)


class GetMarketData():
    def __init__(self, name, use_csv, csv_path, use_vol_csv, vol_csv_path, startdate, Divpoints, gen_tenors, gen_vol_tenor_codes, gen_times, gen_strikes, RFrate_code, ticker, volticker):
        self.name=name
        self.use_csv=use_csv
        self.csv_path=csv_path
        self.use_vol_csv=use_vol_csv
        self.vol_csv_path=vol_csv_path              
        self.startdate=startdate
        self.Divpoints=Divpoints
        self.gen_tenors=gen_tenors
        self.gen_times=gen_times
        self.gen_strikes=gen_strikes
        self.RFrate_code=RFrate_code      
        self.gen_vol_tenor_codes=gen_vol_tenor_codes
        self.ticker=ticker
        self.volticker=volticker
        
    #CREATE MARKETDATA DATAFRAME
    def generate_market_data(self):  
        
        if self.use_csv==1:
            
            mdata = pd.read_csv(self.csv_path, index_col=[0],header=[0,1], skipinitialspace=True)
            mdata.index = pd.to_datetime(mdata.index, format='%Y-%m-%d')#'%d/%m/%Y')
            mdata.dropna(inplace=True)
            
            #RENAME SPOT COLUMN
            mdata = mdata.rename(columns={self.ticker: 'SpotPrice'})
            #FILTER TO START DATE AND SET START_SPOT
            mdata=mdata[self.startdate:]
            self.starting_spot= mdata['SpotPrice','PI'][self.startdate]
            
            #ADD DIVYIELD TO DF
            div_df = mdata.droplevel(1, axis=1).copy()
            div_df = div_df.loc[:, 'SpotPrice']
            mdata['DivYield'] = qnt.calcdivyield(div_df, self.Divpoints )
            
            #CALC GENERIC DATE FORWARDS TO DF
            fwd_df = mdata.droplevel(1, axis=1).copy()           
            for i in range(0,len(self.gen_tenors)):
                mdata[self.gen_tenors[i]+'_fwd']=fwd_df.apply(lambda x: qnt.fwdpricecalc( x['SpotPrice'],   x[self.RFrate_code + self.gen_tenors[i]]/100   ,  x['DivYield'], self.gen_times[i]), axis=1)
          
            #BUILD THE YIELD CURVES DF
            tmp_yield_df = mdata.droplevel(1, axis=1).copy()
            yield_bbgcode_list=[]
            for i in range(0,len(self.gen_tenors)):
                yield_bbgcode_list.append(self.RFrate_code + self.gen_tenors[i])
            self.yield_df = tmp_yield_df.loc[:, yield_bbgcode_list ]
            
            #COPY THE MDATA FRAME BEFORE DROPLEVEL
            self.vdata = mdata.copy() 
        
            mdata = mdata.droplevel(1, axis=1) 
            
            #BUILD THE SABR PARAMETERS DF
            if self.use_vol_csv==1:
                self.SABR_df = pd.read_csv(self.vol_csv_path, index_col=[0])
                self.SABR_df.index = pd.to_datetime(self.SABR_df.index, format='%Y-%m-%d')
                #filter down for speed while testing - NOTE THIS SEEMS TO BE MM/DD/YY not sure why
                self.SABR_df=self.SABR_df[self.startdate:]
            else:
                Sabr_initial_guess=[0.3, -0.7, 1.5]
                self.SABR_df = qnt.bulk_sabr_fit_refinitiv(self.vdata, self.gen_tenors, self.gen_vol_tenor_codes, self.gen_times, self.gen_strikes,  self.volticker, Sabr_initial_guess[0],Sabr_initial_guess[1],Sabr_initial_guess[2] )
                print(self.vdata)
                print(self.SABR_df)
        else:
             pass  
        return mdata


# Percentage Spot Grid Function
def genspotlist(HighestStrike, LowestStrike, NumberOfStrikeBuckets):
    spotlist = [LowestStrike]
    riskspot = LowestStrike
    if HighestStrike > LowestStrike and NumberOfStrikeBuckets > 0:
        while riskspot < HighestStrike:
            riskspot += (HighestStrike - LowestStrike) / NumberOfStrikeBuckets
            spotlist.append(riskspot)
    return spotlist

# Generate list of time observations Function
def gentimelist(Maturity, today, NumberOfTimeBuckets):
    timelist = [today]
    risktime = today
    if Maturity > today and NumberOfTimeBuckets > 0:
        while risktime + timedelta(days=((Maturity - today).days / NumberOfTimeBuckets)) < Maturity:
            risktime += timedelta(days=((Maturity - today).days / NumberOfTimeBuckets))
            timelist.append(risktime)
    timelist.append(Maturity + timedelta(days=-1))
    timelist.append(Maturity)
    return timelist



def tab5chart(spots, times, risktype,SpotPrice,weights,absstrikelist,TTElist,todaysdate,volslist,
              rfratelist,divyieldlist,cplist,OTCVOLUME   ):



    #DO A LIST HERE
    for i in range(0, len(risktype)):
        riskstring=risktype[i]

    
        # Create DataFrame of risk with spot AS PERCENTAGE  -- NB ITS SCALED BY 100!!!!!!
        riskgrid = pd.DataFrame()
    
        for y in times:
            # timetoexpiry=max(((Maturity)-y).days/365),0.000000001))
            # times.append(timetoexpiry)
            for i in spots:
                optrisk = []
                strrisk = 0
                for w in range(0, len(weights)):
                    optrisk.append(weights[w] * qnt.black_scholes((i / 100) * SpotPrice, absstrikelist[w],
                                                               max((TTElist[w]-(y-todaysdate).days/365), 1/365), volslist[w]/100,
                                                               rfratelist[w]/100, divyieldlist[w]/100, cplist[w], riskstring,0))
    
                strrisk = sum(optrisk)
                riskgrid.loc[i, y] =  (strrisk * OTCVOLUME)
    
    
        #SET INDEX VALUES UNIT SPOT SO WE CAN USE IN MAP CALCS
        riskgrid['Spot']=spots
        riskgrid['Spot']=riskgrid['Spot']*SpotPrice/100
        riskgrid.set_index("Spot", inplace=True)
    
    
    
        #APPLY MAPS TO SELECT RISK FORMAT
        if riskstring=="P":
            textdesc="Strat Price"
        elif riskstring=="D":
            riskgrid=riskgrid.apply(lambda x: x * x.name/1000000 ,axis=1)
            textdesc="Delta(M)"
        elif riskstring=="G":
            riskgrid=riskgrid.apply(lambda x: x*x.name*x.name / (100000000), axis=1)
            textdesc="Gamma(MGbp)"
        elif riskstring=="V":
            riskgrid=riskgrid.applymap(lambda x: x / (1000000))
            textdesc="Vega(M)"
        elif riskstring=="T":
            riskgrid=riskgrid.applymap(lambda x: int(x / (1)))
            textdesc="Theta"
        elif riskstring=="R":
            riskgrid=riskgrid.applymap(lambda x: x / (1000000))
            textdesc="Rho(M)"
    
    
    
        # switch to Abs spots for chart if that spottype is selected
    
        riskgrid['%Spot']=spots
        riskgrid.set_index("%Spot", inplace=True)
    
        print(textdesc)
        print(riskgrid)
    
        #SAVE DF TO DATABASE
        
        
        # OUTPUT CHART ONTO TAB3
        plt.style.use('dark_background')
        figure = plt.Figure(figsize=(9, 6), dpi=100)
        ax = figure.add_subplot(111)
        #chart_type = FigureCanvasTkAgg(figure, self.tab5f2)
       #chart_type.get_tk_widget().grid(column=0, row=2)  # pack()
        riskgrid.plot(kind='line', legend=True, ax=ax)
        ax.set_xlabel("Spot")
        ax.set_ylabel(textdesc)
        ax.set_title(textdesc + " with Spot and T")
    
    
        #3D CHART
        plt.style.use('dark_background')
        dfmtmsurface = riskgrid
        dfzaxis = textdesc
        dftitle = textdesc + " with T and S"
        # convert dataframe to daystoexpiry from date (cant plot date on 3D)
        toplotdays = pd.DataFrame(dfmtmsurface.copy())
        newcols = list(toplotdays.columns)
        newcols = list((i - todaysdate).days for i in newcols)
        toplotdays.columns = newcols
        df = pd.DataFrame(toplotdays)
        df = df.transpose()
        x = df.columns
        y = df.index
        X, Y = np.meshgrid(x, y)
        Z = df
        fig1 = plt.figure(figsize=(9, 6), dpi=100)
        ax1 = fig1.add_subplot(111, projection='3d')
        #chart_type3d = FigureCanvasTkAgg(fig1, self.tab5f3)
        #chart_type3d.get_tk_widget().grid(column=0, row=0)
        ax1.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap='viridis', edgecolor='none')
        ax1.mouse_init()
        ax1.set_xlabel("Spot")
        ax1.set_ylabel("Days Forward")
        ax1.set_zlabel(dfzaxis)
        ax1.set_title(dftitle)
    





class OptionRisk(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="OptionRisk")
        """ Initialize all the required constants and data here """

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            

            #Calculate the risk exposure of a single option strategy.

            #Check we can use list inputs.



            #================================================         INPUT PARAMETERS           ================================================================================

            todaysdate = date.today()
            print(todaysdate)

            ticker='DJES50I'
            volticker='GXEMONEY'

            marketdata_startdate = todaysdate + timedelta(days=-20) #'2020-01-01'
            marketdata_startdate=marketdata_startdate.strftime("%Y-%m-%d")


            #REFINITIV CODES
            fieldList = ['PI','O1','O3','OY','IR']
            underlyinglist = 'DJES50I,GXEMONEY80,GXEMONEY90,GXEMONEY95,GXEMONEY100,GXEMONEY105,GXEMONEY110,GXEMONEY115,OIEUR1M,OIEUR3M,OIEUR1Y'


            #ENTER DIVIDENDS
            Divpoints={'2007': 146.5,'2008': 158.6,'2009': 115.7,'2010': 112.7,'2011': 124.3,'2012': 115.6,'2013': 109.8,
                       '2014': 114.1,'2015': 114.9,'2016': 118.4,'2017':116.9 ,'2018': 125.6, '2019': 122.1, '2020': 86.4 }

            gen_tenors=["1M","3M","1Y"]
            gen_vol_tenor_codes = ['O1', 'O3', 'OY']
            gen_times=[0.084,0.25,1]
            gen_strikes=[80, 90, 95, 100, 105, 110, 115]

            RFrate_code='OIEUR'

            base_mdata_file = "/data/OptionRefinitivDataFile.csv"

            #TO PICK UP THE OTHER INPUT FILES IF USING CSVs
            #mdata_file = "C:/Users/ralphm/DemoPython/Inputs/MDATA_DF.csv"
            SABRdata_file = "/data/SABR_DF.csv"


            output_dir = "/data/"



            # LOAD IN PRICES FROM REFINITIV
            # ------------------------------------------------------------------------------

            # ------------------------------------------------------------------------------
            if __name__ == '__main__':
                dsAdapter = DSWSAdapter('ZPML029', 'CLICK223')  # Ralph


                Refin_data = dsAdapter.GetPriceHistForTicker(underlyinglist, fieldList, marketdata_startdate)

            #remove multiindex
            #Refin_data.columns=Refin_data.columns.droplevel(1)
            Refin_data.reset_index(inplace=True)
            Refin_data=Refin_data.rename(columns={'Dates': 'Date'})
            Refin_data.set_index('Date',inplace=True)


            Refin_data.index = pd.to_datetime(Refin_data.index, format='%Y-%m-%d')
            #remove non-priced columns
            Refin_data = Refin_data.dropna(how='all', axis=1)

            #SAVE BASE REFINITIV DATA TO CSV
            Refin_data.to_csv(base_mdata_file)


            #Set start date to last available MDATA point
            startdate = Refin_data.index[-1]




            #INITIATE MARKET DATA CLASS======================================

            market_data = GetMarketData('my_market_data', 1, base_mdata_file,  0, SABRdata_file, startdate,
                                        Divpoints, gen_tenors, gen_vol_tenor_codes, gen_times, gen_strikes, RFrate_code, ticker, volticker)

            #LOAD IN MARKET DATA
            mdata = market_data.generate_market_data()

            vdatadebug=market_data.vdata


            #SET UP OPTION STRATEGY GLOBAL VARIABLES

            OTCVOLUME = 10000
            
            weightstring = sys.argv[2]
            string_to_list=weightstring.split(',')
            for i in string_to_list:
                i=int(i)
                
            weights = string_to_list
            absstrikelist = [3000,3500]
            cpdesclist=['P',"C"]
            cplist=[-1,1]#([i for i in cpdesclist if i=='P' else 1])
            print(cplist)

            TTElist = [0.25, 0.25]

            SpotPrice = mdata['SpotPrice'][-1]
            print(SpotPrice)

            #yieldcurveinterp(df, Ratetenorlist, T, date)

            rfratelist=[1,1]
            divyieldlist=[0.03, 0.03]

            volslist = [30,20]

            minspot=80
            maxspot=120
            steps=20
            spots=genspotlist(maxspot, minspot, steps)


            time_buckets=3
            #calc min maturity
            minmaturity=todaysdate+timedelta(days=min(TTElist)*365)
            #make sure there are not more than daily timebuckets
            days_to_min_maturity=(minmaturity-todaysdate).days
            # ONLY GENERATE TIME BUCKETS UNTIL SHORTEST MATURITY
            gridtime = gentimelist(minmaturity, todaysdate, min(time_buckets,days_to_min_maturity))

            Risklist=['P','D', 'G', 'V', 'T', 'R']

            #CALC RISK
            riskdf=tab5chart(spots, gridtime, Risklist,SpotPrice,weights,absstrikelist,TTElist,todaysdate,volslist,
              rfratelist,divyieldlist,cplist,OTCVOLUME)            
            
    
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(success=True)

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = OptionRisk()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
